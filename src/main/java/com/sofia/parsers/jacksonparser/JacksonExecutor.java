package com.sofia.parsers.jacksonparser;

import com.sofia.model.objectclasses.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static com.sofia.model.Constants.*;
import static com.sofia.parsers.PrintParsed.printList;
import static com.sofia.parsers.jacksonparser.JacksonParser.*;

public class JacksonExecutor {
    private static final Logger LOG = LogManager.getLogger(JacksonExecutor.class);

    public static void readAndWriteJackson() {
        try {
            List<Book> listAllBooks = getObjectWithJackson(JSON_TO_PARSE);
            printList(listAllBooks);
            LOG.info("Writing to JSON (with Jackson) .....");
            writeObjectWithJackson(listAllBooks, PATH_TO_WRITE_JACKSON);
            LOG.info("Writing done " + PATH_TO_WRITE_JACKSON);
            LOG.info("Certain filed: ");
        } catch (FileNotFoundException e) {
            LOG.error(FILE_NOT_FOUND_EXCEPTION + e);
            e.printStackTrace();
        } catch (IOException e) {
            LOG.error(USUAL_EXCEPTION + e);
            e.printStackTrace();
        }
    }
}
