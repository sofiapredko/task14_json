package com.sofia.parsers.jacksonparser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sofia.model.objectclasses.Book;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.sofia.model.Constants.*;

public class JacksonParser {
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static List<Book> getObjectWithJackson(String pathToJson) throws IOException {
        byte[] jsonData = Files.readAllBytes(Paths.get(pathToJson));
        List<Book> listBooks = objectMapper.readValue(jsonData, new TypeReference<List<Book>>() {
        });
        return listBooks;
    }

    public static void writeObjectWithJackson(List<Book> books, String path) throws IOException {
        objectMapper.writeValue(new File(PATH_TO_WRITE_JACKSON), books);
    }
}
