package com.sofia.parsers;

import com.sofia.model.BookComparator;
import com.sofia.model.objectclasses.Book;
import com.sofia.parsers.jacksonparser.JacksonExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class PrintParsed {
    private static final Logger LOG = LogManager.getLogger(JacksonExecutor.class);

    public static void printList(List<Book> books) {
        books.sort(new BookComparator());
        LOG.info("Books:");
        books.forEach(book -> System.out.println(book));
    }
}
