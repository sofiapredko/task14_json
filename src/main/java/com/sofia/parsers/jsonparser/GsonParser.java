package com.sofia.parsers.jsonparser;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.sofia.model.objectclasses.Book;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

public class GsonParser {
    private static final Type GET_ALL_IN_LIST = new TypeToken<List<Book>>(){}.getType();

    public static List<Book> getObjectFromJson(String json) throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(json));
        return gson.fromJson(reader, GET_ALL_IN_LIST);
    }

    public static void writeObjectToJson(List<Book> books, String path) throws IOException {
        Gson gson = new Gson();
        String json = gson.toJson(books);
        FileWriter writer = new FileWriter(path);
        writer.write(json);
        writer.close();
    }
}
