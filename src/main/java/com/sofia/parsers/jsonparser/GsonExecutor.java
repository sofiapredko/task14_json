package com.sofia.parsers.jsonparser;
пше
import com.sofia.model.objectclasses.Book;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.sofia.model.Constants.*;
import static com.sofia.parsers.PrintParsed.printList;

public class GsonExecutor {
    private static final Logger LOG = LogManager.getLogger(GsonExecutor.class);

    public static void readAndWriteJson() {
        try {
            List<Book> books = GsonParser.getObjectFromJson(JSON_TO_PARSE);
            printList(books);
            LOG.info("Writing to JSON.....");
            GsonParser.writeObjectToJson(books, PATH_TO_WRITE_GSON);
            LOG.info("Writing done " + PATH_TO_WRITE_GSON);
        } catch (FileNotFoundException e) {
            LOG.error(FILE_NOT_FOUND_EXCEPTION + e);
            e.printStackTrace();
        } catch (IOException e) {
            LOG.error(USUAL_EXCEPTION + e);
            e.printStackTrace();
        }
    }

}
