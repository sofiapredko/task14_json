package com.sofia.view;

import com.sofia.parsers.jacksonparser.JacksonExecutor;
import com.sofia.parsers.jsonparser.GsonExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    private static final Logger LOG = LogManager.getLogger(Menu.class);
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;
    private static final Scanner INPUT = new Scanner(System.in);

    private Menu() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - JSON parser GSON");
        menu.put("2", " 2 - JSON parser Jackson");
        menu.put("0", " 0 - Exit.");

        methodsMenu.put("1", GsonExecutor::readAndWriteJson);
        methodsMenu.put("2", JacksonExecutor::readAndWriteJackson);
        methodsMenu.put("0", this::exitFromProgram);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    private void exitFromProgram() {
        LOG.debug("SUCCESSFULLY END");
        System.exit(0);
    }

    public static void runMenu() {
        String key;
        Menu menu = new Menu();
        while (true) {
            try {
                System.out.println("\n");
                menu.outputMenu();
                System.out.println("Please, select menu point");
                key = INPUT.nextLine();
                System.out.println("\n\n");
                menu.methodsMenu.get(key).start();
            } catch (NullPointerException e) {
                LOG.warn("\nChoose correct choice please");
            } catch (IOException e) {
                LOG.error("\nSomething wrong with files!");
            } catch (ClassNotFoundException e) {
                LOG.error("\nCan't find class");
            } catch (Exception e) {
                LOG.error("ERROR!!!");
            }
        }
    }
}
