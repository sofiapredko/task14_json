package com.sofia.model;

import com.sofia.model.objectclasses.Book;
import java.util.Comparator;

public class BookComparator implements Comparator<Book> {
    @Override
    public int compare(Book book1, Book book2) {
        return Integer.compare(book1.getChars().getPages(), book2.getChars().getPages());
    }
}
