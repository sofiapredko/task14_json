package com.sofia.model;

public class Constants {
    public static final String JSON_TO_PARSE = "src\\main\\resources\\jsonfiles\\books.json";
    public static final String PATH_TO_WRITE_GSON = "src\\main\\resources\\jsonfiles\\booksFromObjectGSON.json";
    public static final String PATH_TO_WRITE_JACKSON = "src\\main\\resources\\jsonfiles\\booksFromObjectJackson.json";
    public static final String FILE_NOT_FOUND_EXCEPTION = "Error!!! Not found";
    public static final String USUAL_EXCEPTION = "Error!!!";

    private Constants(){
    }
}
