package com.sofia.model.objectclasses;

public class Chars {
    private int pages;
    private boolean colourful;
    private String genre;
    private String readers;
    private int index;

    public Chars() {
    }

    public Chars(int pages, boolean colourful, String genre, String readers, int index) {
        this.pages = pages;
        this.colourful = colourful;
        this.genre = genre;
    }

    public int getPages() {
        return pages;
    }

    public String getGenre() {
        return genre;
    }

    public boolean isColourful() {
        return colourful;
    }

    public void setColourful(boolean colourful) {
        this.colourful = colourful;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "Chars{" +
                "pages='" + pages + '\'' +
                ", colourful=" + colourful +
                ", genre='" + genre + '\'' +
                '}';
    }

}
