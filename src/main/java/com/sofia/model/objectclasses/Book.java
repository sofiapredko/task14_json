package com.sofia.model.objectclasses;

public class Book {
    private String title;
    private String author;
    private String type;
    private Chars chars;

    public Book() {
    }

    public Book(String title, String author, Chars chars) {
        this.title = title;
        this.author = author;
        this.chars = chars;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Chars getChars() {
        return chars;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", chars=" + chars +
                '}';
    }

}
